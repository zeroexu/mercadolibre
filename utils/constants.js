import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();
const { port, host, api } = publicRuntimeConfig;

export const constants = {
  APP: {
    name: "Mercadolibre",
    title: "search",
    host: host,
    port: port
  },
  SERVICES: {
    search: `${api}/product/search`,
    item: `${api}/product`
  }
}