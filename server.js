// server.js
require('dotenv').config({ path: process.env.NODE_ENV ? `${__dirname}/env/.env.${process.env.NODE_ENV}` : `${__dirname}/env/.env.development` });
const next = require('next')
const routes = require('./routes')
const app = next({ dev: process.env.NODE_ENV !== 'production' })
const handler = routes.getRequestHandler(app)
const port = process.env.PORT


const { createServer } = require('http')
app.prepare().then(() => {
  createServer(handler).listen(port)
})