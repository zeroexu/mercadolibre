import '../scss/product_cell.scss'
import { constants } from '../../../utils/constants'
const { APP } = constants
const getItemDetailLink = (APP, id) => {
  return `${APP.host}:${APP.port}/item/${id}`
}
const ProductCell = ({ item }) => {
  const { id, price, picture, title, address } = item
  const { state_name } = address
  const { currency, amount, decimals } = price
  return <a href={getItemDetailLink(APP, id)}>
    <div className='product-cell'>
      <div className='thumbnail-container'>
        <img src={picture} />
      </div>
      <div className='product-info'>
        <h2>{`${currency} ${amount}`}</h2>
        <h3>{title}</h3>
      </div>
      <div className='product-location'>
        <h4>{state_name}</h4>
      </div>
    </div>
  </a>
}

export default ProductCell