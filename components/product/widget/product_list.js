import ProductCell from './product_cell'
import '../scss/product_list.scss'

const ProductList = ({ items = [] }) => {
  return <div id="product-list">
    {items.map((item, i) => {
      return <ProductCell item={item} key={i} />
    })}
  </div>
}

export default ProductList