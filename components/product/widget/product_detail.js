import '../scss/product_detail.scss'


const ProductDetail = ({ item }) => {
  const { description, picture, condition, sold_quantity, title, price } = item
  const { currency, amount, decimals } = price

  const descriptionArray = description.split('\n');

  return <div id='product-detail'>
    <div className='thumbnail-container'>
      <img src={picture} />
    </div>
    <div className='product-info'>
      <h5>{`${condition} - ${sold_quantity} vendidos`}</h5>
      <h1>{`${title}`}</h1>
      <h2>{`${currency} ${amount}`}</h2>
      <button>Comprar</button>
    </div>
    <div className='product-description'>
      <h2>Descripcion del producto</h2>
      {descriptionArray.map((line, i) => {
        return <p key={i}>{line}</p>
      })}
    </div>
  </div >
}

export default ProductDetail