import '../scss/breadcrumb.scss'

const BreadCrumb = ({ categories = [] }) => {
  return <div id='breadcrumb'>
    <span>{categories.join(' >> ')} </span>
  </div>
}

export default BreadCrumb