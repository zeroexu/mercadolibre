import React, { useState } from 'react';
import { Link } from '../../../routes'
import '../scss/search.scss'
import CustomInputText from '../../commons/widget/custom_input_text'
import { constants } from '../../../utils/constants'
const { APP } = constants

const getLinkToResult = (APP, word) => {
  return `${APP.host}:${APP.port}/items/search/${word}`
}
const Search = () => {
  const [word, setWord] = useState(' ');
  const searchCallback = (feedback) => {
    setWord(feedback)
  }

  const onEnterCallback = () => {
    window.location = getLinkToResult(APP, word)
  }
  return <div className='search-bar'>
    <div></div>
    <Link
      route={'index'}
    >
      <a><img src={`${APP.host}:${APP.port}/Logo_ML.png`} /></a>
    </Link>

    <CustomInputText
      type={'text'}
      name={'search'}
      callbackOnChange={searchCallback}
      callbackKeyPress={onEnterCallback}
    />

    <a href={getLinkToResult(APP, word)}>
      <button name='submit'>
        <img src={`${APP.host}:${APP.port}/ic_Search.png`} />
      </button>
    </a>
    <div></div>
  </div >
}

export default Search