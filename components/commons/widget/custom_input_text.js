import React, { Component } from 'react'

export default class CustomInputText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { callbackOnChange } = this.props
    callbackOnChange(e.target.value)
  }

  handleKeyPress = (e) => {
    const { callbackKeyPress } = this.props
    if (e.key === 'Enter') {
      callbackKeyPress()
    }
  }

  render() {
    const { name, className, placeholder } = this.props
    return <input
      type='text'
      name={name}
      className={className}
      onChange={this.handleChange}
      placeholder={placeholder}
      onKeyPress={this.handleKeyPress}
    />
  }
}
