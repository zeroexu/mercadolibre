import Head from 'next/head'
import { constants } from '../../../utils/constants'
import '../scss/layout.scss'
const { APP } = constants

const Header = () => {
  return <div>
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      <title>{`${APP.name} ${APP.title}`}</title>
      <link rel="preload" href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet"></link>
    </Head>
  </div >
}

export default Header