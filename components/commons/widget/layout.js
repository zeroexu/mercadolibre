import Meta from './meta'
import Search from '../../search/widget/search'
import BreadCrumb from '../../breadcrumb/view/breadcrumb'


const Layout = ({ filters = [], children }) => {
  return <React.Fragment>
    <Meta />
    <div id='search-container'>
      <Search />
    </div>
    <div id='container'>
      <BreadCrumb categories={filters} />
      {children}
    </div>
  </React.Fragment>
}

export default Layout