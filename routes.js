const routes = require('next-routes')

module.exports = routes()
  .add('static', '/resource')
  .add('index', '/', 'index')
  .add('search', '/items/:slug/:quest')
  .add('item', '/item/:id')