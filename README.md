#Web UI de Test para MercadoLibre


Descripcion
----------------------------

Este proyecto tiene tres paginas
-search
-result
-product detail

Esta realizado con NextJs (ReactJs), que viene con todos los beneficios del
server side rendering, como el SEO o la velocidad de carga de la aplicacion en el cliente

Requerimientos
---------------------------

- React 16.9
- Node 12.12.+
- Npm 6.9.0

IMPORTANTE:
--------------------------
Este proyecto interactua con API que es la segunda parte del proyecto, un API REST
que esta realizado con Nodejs + Express

- https://gitlab.com/zeroexu/MercadoLibre-API

Instalacion
---------------------------

Para instalar solo tienes que ejecutar en el terminal

- npm install
- npm run start 
- npm run dev para el modo desarrollo

En el modo desarrollo el App trabaja por el puerto 5000 y se comunica con el backend por el 5001
En el modo produccion el App trabaja por el puerto 3000 y se comunica con el backend por el 3001

Si deseas puedes configurar tus variables de entorno en al carpeta env
Puedes integrar estos archivos a tu archivo de configuracion de integracion continua para que hagas deployment a tu servidor.


