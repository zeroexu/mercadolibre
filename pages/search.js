import fetch from 'isomorphic-unfetch'
import Layout from '../components/commons/widget/layout'
import ProductList from '../components/product/widget/product_list'
import Error from './_error'
import { constants } from '../utils/constants'
const { SERVICES } = constants

const SearchPage = ({ items = [], filters = [], statusCode }) => {

  if (statusCode !== 200) {
    return <Error statusCode={statusCode} />
  }
  return <Layout filters={filters}>
    <ProductList items={items} />
  </Layout>
}

const getInitialProps = async ({ query, res = {} }) => {
  try {
    const search = query.quest;
    let req = await fetch(`${SERVICES.search}/${search}`)
    const data = await req.json();
    return {
      items: data.items,
      filters: data.categories,
      statusCode: 200
    }
  } catch (error) {
    console.log("error", error)
    if (res)
      res.statusCode = 503
    return { statusCode: 503 }
  }
}
SearchPage.getInitialProps = getInitialProps;

export default SearchPage;