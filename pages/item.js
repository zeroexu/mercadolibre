import fetch from 'isomorphic-unfetch'
import Layout from '../components/commons/widget/layout'
import ProductDetail from '../components/product/widget/product_detail'
import Error from './_error'
import { constants } from '../utils/constants'
const { SERVICES } = constants

const ItemPage = ({ item, statusCode }) => {
  if (statusCode !== 200) {
    return <Error statusCode={statusCode} />
  }

  return <Layout >
    <ProductDetail item={item} />
  </Layout>
}

const getInitialProps = async ({ query, res }) => {
  try {
    const id = query.id;
    let req = await fetch(`${SERVICES.item}/${id}`)
    const data = await req.json();
    return { item: data.item, statusCode: 200 }
  } catch (e) {
    console.log(e)
    res.statusCode = 503
    return { statusCode: 503 }
  }
}
ItemPage.getInitialProps = getInitialProps;

export default ItemPage;