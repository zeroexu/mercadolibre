import CustomLayout from '../components/commons/widget/layout'
import Error from 'next/error'

const index = ({ statusCode }) => {
  return <CustomLayout title="">
    <Error statusCode={statusCode} />
  </CustomLayout>

}

export default index;