const withSass = require('@zeit/next-sass');
const withCSS = require("@zeit/next-css");
require('dotenv').config({ path: process.env.NODE_ENV ? `${__dirname}/env/.env.${process.env.NODE_ENV}` : `${__dirname}/env/.env.development` });

module.exports = withCSS(withSass({
  publicRuntimeConfig: {
    host: process.env.HOST,
    port: process.env.PORT,
    api: process.env.API,
  },
  webpack(config, { isServer }) {
    cssModules: true

    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000
        }
      }
    });
    if (!isServer) {
      config.node = {
        fs: 'empty'
      }
    }
    return config;
  }
}));